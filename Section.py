# -*- coding: utf-8 -*-
#Authorship
__author__ = "Vladimir Vetov"
__email__ = "vladimir.vetov@gmail.com"
__status__ = "Prototype"

import re
from BeautifulSoup import BeautifulSoup
import urllib2
import json


class Section:
    def __init__(self):
        self.sectionsArray=[]
        self.pattern = re.compile('\./\d*\.html')


    def getSectionProtocols(self, sectionNumber):
        req = urllib2.Request("http://results.cik.bg/pvrnr2016/tur1/protokoli_rf/%s/" %(sectionNumber), headers={'User-Agent' : "Magic Browser"})
        protocols = []
        response = urllib2.urlopen(req)
        html = response.read()
        soup = BeautifulSoup(html)
        for link in soup.findAll("a"):

            if self.pattern.match(str(link.get("href"))):
                protocols.append(link.get("href").replace("./","").replace(".html",""))
        return protocols

    def getVotersPerList(self, sectionNumber, protocolNumber):
        req = urllib2.Request("http://results.cik.bg/pvrnr2016/tur1/protokoli_rf/%s/%s.html"%(sectionNumber,protocolNumber), headers={'User-Agent' : "Magic Browser"})
        response = urllib2.urlopen(req)

        html = response.read()

        table_data = [[cell.text for cell in row("td")]
                      for row in BeautifulSoup(html)("tr")]

        votersPerList = 0
        voters = json.loads(json.dumps(dict(table_data),ensure_ascii=False, encoding='utf-8'))
        for key in voters.keys():
            if key.encode("utf-8") == "2. Брой на гласувалите според подписите в списъка, включително и подписите в допълнителната страница (под чертата)":#"3. Брой на гласувалите според намерените в кутията за гласуване пликове":#"2. Брой на гласувалите според подписите в списъка, включително и подписите в допълнителната страница (под чертата)":
                votersPerList = int(voters.get(key))

            if key.encode('utf-8') == "2. Брой на гласувалите според подписите в избирателния списък, включително и подписите в допълнителната страница (под чертата)":#"3. Брой на гласувалите според намерените в кутията за гласуване пликове:":#
                votersPerList = int(voters.get(key))
        return votersPerList

    def getVotesWithoutEnvelopes(self,sectionNumber, protocolNumber):
        req = urllib2.Request("http://results.cik.bg/pvrnr2016/tur1/protokoli_rf/%s/%s.html"%(sectionNumber,protocolNumber), headers={'User-Agent' : "Magic Browser"})
        response = urllib2.urlopen(req)
        #time.sleep(0)
        html = response.read()

        table_data = [[cell.text for cell in row("td")]
                      for row in BeautifulSoup(html)("tr")]

        votersPerList = 0
        voters = json.loads(json.dumps(dict(table_data),ensure_ascii=False, encoding='utf-8'))
        for key in voters.keys():
            if key.encode("utf-8") == "4. Брой на намерените бюлетини в кутията без плик":
                votersPerList = int(voters.get(key))

        return votersPerList


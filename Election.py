# -*- coding: utf-8 -*-
#Author
import json
from Section import Section


sections = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32']
#sections = ["32"]
sectionController = Section()

voters=0



for section in sections:

    protocols = None
    print "Current Section Number %s" % section
    protocols = sectionController.getSectionProtocols(section)
    for protocol in protocols:
       # print "Current Protocol Number %s" %protocol
        voters  = voters + int(sectionController.getVotersPerList(section, protocol))
       # print "Current Voters %s" %voters


print "Overall voters"
print voters
